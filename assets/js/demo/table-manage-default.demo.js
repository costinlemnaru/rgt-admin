/*
Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Version: 4.2.0
Author: Sean Ngu
Website: http://www.seantheme.com/color-admin-v4.2/admin/
*/

var handleDataTableDefault = function() {
	"use strict";

	// get request 
	function getData( type ) {
		var request = $.ajax({
			method: 'GET',
			url: 'http://ec2-35-156-202-70.eu-central-1.compute.amazonaws.com/v1/race/calendar',
			headers: {
				uuid: '00000001-0000-0000-0000-000000000000',
				token: '0000000000000000000000000000000000000000000000000000000000000001'
			}
		});

		request.done(function( object ) {
			// continue just if status is 200
			if (object.status != 200)
				return false;

			var rowItem;
			switch(type) {
			    case 'other':
			        rowItem = object.data.other;
			        break;
			    case 'past':
			        rowItem = object.data.past;
			        break;
			    case 'subscribed':
			        rowItem = object.data.subscribed;
			        break;
			    default:
			        rowItem = object.data.other;
			}

			for (var i = 0; i < rowItem.length; i++) {
				var name 		= rowItem[i].name;
				var start_at 	= moment( rowItem[i].start_at ).format('hh:mm DD/MM/YYYY');
				var road_label 	= rowItem[i].road_label;
				var laps		= ((rowItem[i].road_shape != 0) ? rowItem[i].laps : 'N/A');
				var subscribers = rowItem[i].subscribers;

				dataTable.row.add( [
					name,
					start_at,
					road_label,
					laps,
					subscribers,
				] ).draw( false );
			}
		});
	}

	// update default list
	if ($('#data-table-default').length !== 0) {
		var dataTable = $('#data-table-default').DataTable({
			responsive: true
		});

		getData();
	}

	// update list by tab
	$('.racesList').on('click', function(){
		var dataList = $(this).attr('data-list');

		var dataTable = $('#data-table-default').DataTable();
		var rows = dataTable
			.rows()
			.remove()
			.draw();

		getData(dataList);
	});
};

var TableManageDefault = function () {
	"use strict";
	return {
		//main function
		init: function () {
			handleDataTableDefault();
		}
	};
}();